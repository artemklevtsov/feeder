#include <cpp11.hpp>
#include <pugixml.hpp>
#include "parse_atom.h"
#include "parse_rss.h"
#include "utils.h"

using namespace cpp11::literals;

[[cpp11::register]]
cpp11::list parse_feed(cpp11::r_string path) {
  pugi::xml_document doc;
  pugi::xml_parse_result res = doc.load_file(cpp11::as_cpp<const char*>(path), pugi::parse_escapes);
  if (!res) {
    cpp11::stop("XML parsing error: %s", res.description());
  }

  std::string root_name(doc.first_child().name());
  if (root_name.compare("feed") == 0) {
    return parse_atom_feed(doc);
  } else if (root_name.compare("rss") == 0) {
    return parse_rss_feed(doc);
  } else {
    cpp11::stop("Not supported feed type.");
  }
}
