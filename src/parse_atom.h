#pragma once

#include <cpp11.hpp>
#include <pugixml.hpp>

cpp11::data_frame parse_atom_feed(const pugi::xml_document&);
