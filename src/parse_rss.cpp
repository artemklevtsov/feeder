#include "parse_rss.h"
#include "utils.h"

using namespace cpp11::literals;

cpp11::data_frame parse_rss_feed(const pugi::xml_document& doc) {
  pugi::xml_node channel = doc.child("rss").child("channel");
  cpp11::writable::strings res_title;
  cpp11::writable::strings res_link;
  cpp11::writable::strings res_desc;
  cpp11::writable::doubles res_date;
  std::string dtm_fmt("%a, %d %b %Y %T %Z");
  for (const pugi::xml_node& node: channel.children("item")) {
    res_title.push_back(node.child("title").text().get());
    res_link.push_back(node.child("link").text().get());
    res_desc.push_back(node.child("description").text().get());
    res_date.push_back(parse_date_time(node.child("pubDate").text().get(), dtm_fmt));
  }

  cpp11::writable::data_frame res({
    "title"_nm = res_title,
      "link"_nm = res_link,
      "date"_nm = res_date,
      "description"_nm = res_desc,
  });

  res.attr("channel_link") = channel.child("link").text().get();
  res.attr("channel_title") = channel.child("title").text().get();
  res.attr("channel_description") = channel.child("description").text().get();
  res.attr("channel_updated") = parse_date_time(channel.child("lastBuildDate").text().get(), dtm_fmt);

  return res;
}
