#include "parse_atom.h"
#include "utils.h"

using namespace cpp11::literals;

cpp11::data_frame parse_atom_feed(const pugi::xml_document& doc) {
  pugi::xml_node feed = doc.child("feed");
  cpp11::writable::strings res_title;
  cpp11::writable::strings res_link;
  cpp11::writable::strings res_desc;
  cpp11::writable::doubles res_date;
  std::string dtm_fmt("%FT%X%Z");
  for (const pugi::xml_node& node: feed.children("entry")) {
    res_title.push_back(node.child("title").text().get());
    res_link.push_back(node.child("link").attribute("href").value());
    std::string desc = node.child("summary").text().get();
    if (desc.empty()) {
      desc = node.child("content").text().get();
    }
    res_desc.push_back(desc);
    std::string pub_dt = node.child("published").text().get();
    if (pub_dt.empty()) {
      pub_dt = node.child("updated").text().get();
    }
    res_date.push_back(parse_date_time(pub_dt, dtm_fmt));
  }

  cpp11::writable::data_frame res({
    "title"_nm = res_title,
      "link"_nm = res_link,
      "date"_nm = res_date,
      "description"_nm = res_desc
  });

  res.attr("channel_link") = feed.child("link").attribute("href").value();
  res.attr("channel_title") = feed.child("title").text().get();
  res.attr("channel_description") = feed.child("subtitle").text().get();
  res.attr("channel_updated") = parse_date_time(feed.child("updated").text().get(), dtm_fmt);
  return res;
}
