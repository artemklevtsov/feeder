#include <iostream>
#include <string>
#include <chrono>
#include <sstream>
#include <locale>
#include <pugixml.hpp>
#include <date/date.h>
#include <cpp11.hpp>
#include "utils.h"


double parse_date_time(const std::string& s, const std::string& fmt) {
  if (s.empty()) {
    return NAN;
  }
  std::istringstream ss(s);
  ss.imbue(std::locale("C"));
  date::sys_seconds tp;
  ss >> date::parse(fmt, tp);
  if (ss.fail()) {
    cpp11::stop("Can not parse date '%s' with format '%s'.", s.c_str(), fmt.c_str());
  }
  return std::chrono::duration_cast<std::chrono::seconds>(tp.time_since_epoch()).count();
}

cpp11::doubles to_posixct(cpp11::writable::doubles x) {
  x.attr("class") = {"POSIXct", "POSIXt"};
  x.attr("tzone") = "UTC";
  return x;
}
